use std::env;
use std::io;
use std::path::Path;
use std::str::FromStr;
///for colors
extern crate term;
use std::io::prelude::*;
use self::term::color::*;

pub fn print() {
	let mut t = term::stdout().unwrap();
	let cwd = env::current_dir()
					.unwrap()
					.into_os_string();

	let username =	env::var("USER").unwrap_or(
						env::var("USERNAME").unwrap_or(
							"user_name?".to_string()
					));

	let hostname =	env::var("HOSTNAME").unwrap_or(
						env::var("HOSTNAME").unwrap_or(
							"host_name?".to_string()
					));
	t.fg(GREEN).unwrap();
	write!(&mut t,"{}",username);
	t.fg(BLACK).unwrap();
	write!(&mut t,"@").unwrap();
	t.fg(RED).unwrap();
	write!(&mut t,"{}",hostname).unwrap();
	t.fg(BLUE).unwrap();
	write!(&mut t,"::").unwrap();
	t.fg(YELLOW).unwrap();
	write!(&mut t,"{}",cwd.to_str().unwrap_or("current_dir")).unwrap();
	t.fg(WHITE).unwrap();
	write!(&mut t,">> ").unwrap();
	t.reset().unwrap();
	t.flush().unwrap();
}


pub fn getline() -> String {
	let mut line = String::new();
	let input = io::stdin();
	if input.lock().read_line(&mut line).is_err() {
		println!("error on input");
		line = String::from_str("").unwrap();
	}
	line
}

pub fn flush()	{
	io::stdout().flush();
}
