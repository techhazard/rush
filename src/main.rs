extern crate rush;
use self::rush::commands::*;

fn main() {

	rush::init();

	let mut line:String;
	let mut commands;
	loop {
		//print prompt 
		rush::prompt::flush();
		rush::prompt::print();
		line = rush::prompt::getline();
		//exit on Ctrl-D
		if &line[..] == "" { println!("");break; }

		//remove \n at end
		line.pop();
		commands = rush::commands::parse(&line).unwrap()
							.build().unwrap()
							.run();
	}
	println!("end of RUst SHell");
}
