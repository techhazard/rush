// rush::commands::*
use std::process as ps;


/// this checks if the string is a valid (set of) command(s)
/// this returns a Result  with a vec of stmnts's (String's)
pub fn parse(inputstr:&String) ->  Result<Vec<String>,&'static str> {

    let mut stmnts : Vec<String> = Vec::new();

    println!("placeholder: parse inputstr");

    for substr in inputstr.split(';') {
        stmnts.push(substr.to_string());
    }

    for x in &stmnts {
        println!("stmntsx {}",x);
    }

    if stmnts.len() == 0 {
        return Err("error parsing string")
    }
    Ok(stmnts)

}

fn doestmnts(stmnts : &Vec<String>) -> bool {

    println!("placeholder parse stmnts");

    for stmnt in stmnts {

        // parse stmnt:
        // 	split on && and ||
        //
        // run stmnt and compare logic (if any)
        // return true or false
        doestmnt(stmnt);



    }
    true
}


fn doestmnt(stmnt :&String) -> Result<bool, &'static str>{

    println!("placeholder doestmnt");
    let commands : Vec<String> = stmnt.split('|').map(|x| x.to_string()).collect::<Vec<_>>();
    if commands.len() > 0 {
        doecommands(commands);
    } else {
        return Err("asdfdsaf")
    }
    Ok(true)
}


fn doecommand(command: String) {

    println!("placeholder doecommand");
}

fn doecommands(commands: Vec<String>) {

    println!("placeholder doecommands");
}

//traits && impl for chaining
pub trait RushBuild {
    fn build(&self) -> Result<Vec<ps::Command>, &'static str>;
}
pub trait RushRun {
    fn run(&self)->bool;
}
/// runs the commands specified
impl RushRun for Vec<ps::Command> {
    fn run(&self) -> bool {
        println!("placeholder: run command");
        true
    }
}




impl RushBuild for Vec<String> {

    fn build(&self) ->  Result<Vec<ps::Command>, &'static str> {

        println!("placeholder: build command");
        let v :Vec<ps::Command> = Vec::new();
        Ok(v)
    }
}
